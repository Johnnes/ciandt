package application;



import java.util.Random;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        Random random = new Random();
        Integer numberToGuess = random.nextInt(99) + 1;

        int guess = -1;
        int tries = 0;

        System.out.println("Input a number:");
        while (numberToGuess != guess) {

            guess = sc.nextInt();
            tries++;
            if (guess > numberToGuess)
                System.out.println("Too High! Try again: ");
            if (guess < numberToGuess)
                System.out.println("Too low! Try again: ");
        }



        System.out.println("You got the number in: " + tries + " tries.");





        sc.close();

    }

}
