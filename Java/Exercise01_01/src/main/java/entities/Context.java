package entities;

public class Context {

    private CharacterCounter charCounter;

    public Context(CharacterCounter characterCounter) {
        this.charCounter = characterCounter;
    }


    public void execute(PhraseVariables phrase){
        if(phrase.getPhrase().compareTo("exit") == 0)
            System.exit(0);
        else {
            try {
                System.out.println(
                        "Type: " + charCounter + "| " + "Amount: " + charCounter.characterCounter(phrase));
            }catch (IllegalArgumentException e){
                System.out.printf("Oh no! That's embarrassing, but something wrong happened: " + e.getMessage());
            }
        }
    }
}
