package entities;


public class CountVowels implements CharacterCounter {

    @Override
    public Integer characterCounter(PhraseVariables phraseToCount) {

        String vowels = phraseToCount.getPhrase().replaceAll( "(?i)[^aeiou]+", "" );

        return vowels.length();
    }

    @Override
    public String toString() {
        return "Vowels \t\t";
    }
}
