package entities;


public class CountSpecialChar implements CharacterCounter {


    @Override
    public Integer characterCounter(PhraseVariables phraseToCount) {

        String especial = phraseToCount.getPhrase().replaceAll( "(?i)[^\\d\\w]+", "" );

        return phraseToCount.getPhrase().length() - especial.length();
    }

    @Override
    public String toString(){
        return "Special Char \t";
    }

}
