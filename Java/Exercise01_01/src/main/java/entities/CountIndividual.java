package entities;


import java.util.HashMap;
import java.util.Map;

public class CountIndividual implements CharacterCounter{



    @Override
    public Integer characterCounter(PhraseVariables phraseVariables) {

        String phrase = phraseVariables.getPhrase().replaceAll( "(?i)[^abcdefghijklmnopqrstuvwxyz]+", "" );

        int len = phrase.length();
        Map<Character, Integer> numChars = new HashMap<Character, Integer>(Math.min(len, 26));

        for (int i = 0; i < len; ++i)
        {
            char charAt = phrase.charAt(i);

            if (!numChars.containsKey(charAt))
            {
                numChars.put(charAt, 1);
            }
            else
            {
                numChars.put(charAt, numChars.get(charAt) + 1);
            }
        }

        System.out.println(phrase);
        System.out.println(numChars);

        return phrase.length();
    }


    @Override
    public String toString(){
        return "Individual \t";
    }
}
