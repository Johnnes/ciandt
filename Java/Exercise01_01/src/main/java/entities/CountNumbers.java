package entities;


public class CountNumbers implements CharacterCounter {

    @Override
    public Integer characterCounter(PhraseVariables phraseToCount) {

        String numbers = phraseToCount.getPhrase().replaceAll( "(?i)[^1234567890]+", "" );

        return numbers.length();

    }

    @Override
    public String toString() {
        return "Numbers \t\t";
    }
}
