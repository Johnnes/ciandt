package entities;

public interface CharacterCounter {

    Integer characterCounter(PhraseVariables phraseToCount);


}
