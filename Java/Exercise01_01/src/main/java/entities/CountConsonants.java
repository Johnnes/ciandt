package entities;


public class CountConsonants implements CharacterCounter {


    @Override
    public Integer characterCounter(PhraseVariables phraseToCount) {

        String consonants = phraseToCount.getPhrase().replaceAll( "(?i)[^bcdfghjklmnpqrstvwxyz]+", "" );

        return consonants.length();
    }

    @Override
    public String toString() {
        return "Consonants \t";
    }
}
