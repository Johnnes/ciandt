package entities;


public class PhraseVariables {

    private String phrase;

    public PhraseVariables(String phrase) {
        this.phrase = phrase.toLowerCase();
    }

    public String getPhrase() {
        return phrase;
    }
}
