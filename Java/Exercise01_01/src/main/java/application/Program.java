package application;

import entities.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        List<CharacterCounter> phrasesList = new ArrayList<>();

        System.out.println("Enter a phrase: \n");
        PhraseVariables phrase = new PhraseVariables(sc.nextLine());
//        PhraseVariables phrase = new PhraseVariables("This phrase has 18 vowels, 38 consonants, 14 special characters and 7 numbers");
        System.out.println("[" + phrase.getPhrase() + "]\n");


        phrasesList.add(new CountNumbers());
        phrasesList.add(new CountVowels());
        phrasesList.add(new CountConsonants());
        phrasesList.add(new CountSpecialChar());
        phrasesList.add(new CountIndividual());

        for (CharacterCounter stringToCount : phrasesList) {
            Context context = new Context(stringToCount);
            context.execute(phrase);
        }


        sc.close();

    }

}
