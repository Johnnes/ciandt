package entities;

public class Multiplication implements CalculateInterface {

    @Override
    public Double calculate(Parameters parameters) {
        return parameters.getFirstTerm() * parameters.getSecondTerm();
    }
}
