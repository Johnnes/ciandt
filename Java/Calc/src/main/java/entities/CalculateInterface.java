package entities;

public interface CalculateInterface {

    Double calculate(Parameters parameters);

}
