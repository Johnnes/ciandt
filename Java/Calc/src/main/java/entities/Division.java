package entities;


public class Division implements CalculateInterface {


    @Override
    public Double calculate(Parameters parameters) {
        return parameters.getFirstTerm() / parameters.getSecondTerm();
    }
}
