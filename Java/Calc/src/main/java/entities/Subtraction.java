package entities;

public class Subtraction implements CalculateInterface {


    @Override
    public Double calculate(Parameters parameters) {
        return parameters.getFirstTerm() - parameters.getSecondTerm();
    }
}
