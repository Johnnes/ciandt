package entities;

import java.util.ArrayList;
import java.util.List;

public class Context {

    List<CalculateInterface> calculateInterfaceList = new ArrayList<>();

    public Context(CalculateInterface operation){
        calculateInterfaceList.add(operation);
    }

    public void execute(Parameters parameters){

        for(CalculateInterface interf : calculateInterfaceList){
            Double res = interf.calculate(parameters);
            System.out.println("Operation: " + interf + "\nResult: " + res);
        }
    }



}
