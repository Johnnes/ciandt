package entities;

public class Parameters {

    private Double firstTerm;
    private Double secondTerm;
    private String operation;
    private String symbol;

    private String mathOperators[] = {"+", "-", "*", "/"};

    public Parameters() {

    }

    public void splitOperators() {
        Integer position = 0;

        for(String op : mathOperators){
            position = op.indexOf(op);
        }

    }

    public Double getFirstTerm() {
        return firstTerm;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation.replace(" ", "");
    }

    public void setFirstTerm(Double firstTerm) {
        this.firstTerm = firstTerm;
    }

    public Double getSecondTerm() {
        return secondTerm;
    }

    public void setSecondTerm(Double secondTerm) {
        this.secondTerm = secondTerm;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "Parameters{" +
                "firstTerm=" + firstTerm +
                ", secondTerm=" + secondTerm +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}
