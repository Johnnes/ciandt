package application;

import sequences.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        List<CalculateInterface> list = new ArrayList<>();

        list.add(new RecursiveFibonacci());
        list.add(new IterativeFibonacci());
        list.add(new RecursiveFactorial());

        System.out.println("Which value to calculate? ");
        long value = sc.nextLong();


        for (CalculateInterface calculateInterface : list){
            Context context = new Context(calculateInterface);
            context.execute(value);
        }

    }
}
