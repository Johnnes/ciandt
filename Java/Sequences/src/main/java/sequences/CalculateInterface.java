package sequences;

public interface CalculateInterface {

    long calculate(long value);

}
