package sequences;

public class IterativeFibonacci implements CalculateInterface {

    @Override
    public long calculate(long value){
        int current = 0;
        int last = 0;

        for (int i = 1; i <= value; i++){
            if (i == 1){
                current = 1;
                last = 0;
            }else{
                current += last;
                last = current - last;
            }
        }

        return current;
    }

}
