package sequences;

public class RecursiveFibonacci implements CalculateInterface {

    public long calculate(long value){
        if (value < 2){
            return value;
        }else{
            return calculate(value -1) + calculate(value -2);
        }
    }

}
