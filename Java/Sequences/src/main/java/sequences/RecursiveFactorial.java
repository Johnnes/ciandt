package sequences;

public class RecursiveFactorial implements CalculateInterface {

    @Override
    public long calculate(long value){
        if (value <= 1)
            return 1;
        else
            return value * calculate(value -1);
    }

}
