package sequences;

import java.util.ArrayList;
import java.util.List;

public class Context {

    List<CalculateInterface> sequences = new ArrayList<>();

    public Context(CalculateInterface sequence){
        sequences.add(sequence);
    }

    public void execute(long value){

        for(CalculateInterface seq : sequences){
            long res = seq.calculate(value);
            System.out.println("Sequence = [" + seq +"], value = [" + value + "], result = [" + res + "]");
        }

    }

}
