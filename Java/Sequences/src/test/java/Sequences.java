import org.junit.Test;
import sequences.IterativeFibonacci;
import sequences.RecursiveFactorial;
import sequences.RecursiveFibonacci;

public class Sequences {

    @Test
    public void iterativeFib_Pos_15(){
        IterativeFibonacci iterativeFibonacci = new IterativeFibonacci();

        long result = iterativeFibonacci.calculate(15);
        assert result == 610;
    }

    @Test
    public void recursiveFib_Pos_14(){
        RecursiveFibonacci recursiveFibonacci = new RecursiveFibonacci();

        long result = recursiveFibonacci.calculate(14);
        assert result == 377;
    }

    @Test
    public void recursiveFac_Pos_5(){
        RecursiveFactorial recursiveFactorial = new RecursiveFactorial();

        long result = recursiveFactorial.calculate(5);
        assert result == 120;
    }


}
